# Documentations collaboratives du SNDS
<!-- SPDX-License-Identifier: MPL-2.0 -->

Bienvenue sur les documentations collaboratives du SNDS.

Ce site héberge:
- la [documentation collaborative du SNDS](/snds/),
- une documentation sur [la standardisation du SNDS au format OMOP](/omop/)


Ces documentations sont en construction, via [ce dépôt GitLab](https://gitlab.com/healthdatahub/documentation-snds).
