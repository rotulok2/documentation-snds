# ADELI - Automatisation des listes
<!-- SPDX-License-Identifier: MPL-2.0 -->

L'automatisation des listes (ADELI) est un répertoire des <link-previewer href="PS.html" text="professionnels de santé" preview-title="PS - Professionnel de santé " preview-text="Les Professionnel de santé (PS) sont des personnes physiques pouvant prescrire ou exécuter des prestations de santé." />.
 
ADELI est progressivement remplacé par le <link-previewer href="RPPS.html" text="RPPS" preview-title="RPPS - Répertoire partagé des professionnels de santé" preview-text="Le répertoire partagé des professionnels de santé (RPPS) est le fichier de référence des professionnels de santé, commun aux organismes du secteur sanitaire et social français. " />, et ne concerne que les catégories de professionnels de santé non-RPPS.

## Numéro ADELI

ADELI attribue un numéro aux professionnels lors de leur enregistrement auprès de leur <link-previewer href="ARS.html" text="ARS" preview-title="ARS - Agence régionale de santé" preview-text="Les Agences régionales de santé (ARS) sont des établissements publics regroupant certaines compétences des services de l’Etat et des services de l’Assurance Maladie." />, qui sert de référence pour les identifier.

::: warning Note
Le numéro ADELI change lorsqu'un professionnels de santé change de région d'exercice.
:::

## Ailleurs sur la documentation
- Fiche thématique sur [le numéro du professionel de santé](../fiches/numero_professionel_sante.md)

## Références

- [Fiche](https://esante.gouv.fr/securite/annuaire-sante/rpps-adeli) sur l'ASIP Santé
- [Page wikipedia](https://fr.wikipedia.org/wiki/Automatisation_des_listes)
