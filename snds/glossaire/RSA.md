# RSA - Résumé de sortie anonyme
<!-- SPDX-License-Identifier: MPL-2.0 -->

Un résumé de sortie anonyme (RSA) est le résultat d'une <link-previewer href="pseudonymisation.html" text="pseudonymisation" preview-title="Pseudonymisation" preview-text="La pseudonymisation est un traitement de données à caractère personnel qui consiste à remplacer les informations &quot;directement&quot; identifiantes par un identifiant pseudonyme artificiel. " /> d’un <link-previewer href="RSS.html" text="résumé de sortie standardisée" preview-title="RSS - Résumé de sortie standardisé" preview-text="Le résumé de sortie standardé (RSS) est constitué de l'ensemble des RUM relatifs au même séjour hospitalier d'un malade dans le secteur MCO." /> pour en retirer les informations directement identifiante.

Cette opération est réalisée par le logiciel Genrsa, pour « générateur de RSA ». 

Le terme "anonyme" a été choisi en 1991, car l'on ne réalisait alors pas les possibilités de croisements, qui permette de réidentifier des individus (cf [limite de la pseudonymisaiton](pseudonymisation.md#limite)).


# Références

- Guide de production PMSI-MCO de l'ATIH, [bas de page 23](https://solidarites-sante.gouv.fr/IMG/pdf/guide_pmsi_mco.pdf#page=23)
- [Données de santé : anonymat et risque de ré-identification](https://drees.solidarites-sante.gouv.fr/IMG/pdf/dss64-2.pdf#page=9), DREES, 2015 - page 9
