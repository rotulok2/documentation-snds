# Section introductive
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette section contient des éléments introductifs à la transformation du SNDS au [format OMOP-CDM, v5.3](https://ohdsi.github.io/CommonDataModel/cdm53.html), déclinés en trois parties :
- [Transformer le SNDS au format OMOP-CDM](snds_omop.md)
- [Périmètre des données standardisées](perimetre_snds.md)
- [Le modèle OMOP-CDM](omop.md)





